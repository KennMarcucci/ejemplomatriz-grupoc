/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Util.LeerMatriz_Excel;
import java.io.IOException;

/**
 *  Clase del negocio para la manipulación de una lista de Calificaciones
 * @author Marco Adarme
 */
public class SistemaCalificaciones {
    
    private Estudiante []listaEstructuras;

    public SistemaCalificaciones() {
    }
    
    
    public SistemaCalificaciones(String nombreArchivo) throws IOException {
        LeerMatriz_Excel miExcel=new LeerMatriz_Excel(nombreArchivo,0);
        String miMatriz[][]=miExcel.getMatriz();
        
        //Normalizar: Pasar de un archivo a un modelo de objetos
        this.listaEstructuras=new Estudiante[miMatriz.length-1];
        crearEstudiantes(miMatriz);
        
        
    }
    
    
    private void crearEstudiantes(String datos[][])
    {
    
        for(int fila=1;fila<datos.length;fila++)
        {
            //Datos para un estudiante
            String nombre="";
            long codigo=0;
            float quices[]=new float[datos[fila].length-2];
            int indice_quices=0;
            for(int columna=0;columna<datos[fila].length;columna++)
            {
            
                
                if(columna==0)
                    codigo=Long.parseLong(datos[fila][columna]);
                else
                {
                    if(columna==1)
                        nombre=datos[fila][columna];
                    else
                    {
                        quices[indice_quices]=Float.parseFloat(datos[fila][columna]);
                        indice_quices++;
                    }
                }
                
            }
            //Creó al estudiante
            Estudiante nuevo=new Estudiante(codigo, nombre);
            nuevo.setQuices(quices);
            //Ingresar al listado de Estudiantes:
            this.listaEstructuras[fila-1]=nuevo;
            
        
        }
          
        
    }
    
    
     public SistemaCalificaciones(int can) {
         
         // T identi[] = new T[tamaño];
         //T es Estudiante
            this.listaEstructuras=new Estudiante[can];
    }
    
    
     //Esto va a cambiar
     public void insertarEstudiante_EnPos0(long codigo,String nombre,  String notas)
     {
         Estudiante nuevo=new Estudiante(codigo, nombre, notas);
         
         this.listaEstructuras[0]=nuevo;
         
     }

    @Override
    public String toString() {
        
        String msg="Mis estudiantes son:\n";
        
        for(Estudiante unEstudiante:this.listaEstructuras)
            msg+=unEstudiante.toString()+"\n";
        
        return msg;
        
    }
     
    /**
     * Obtiene los estudiantes cuyo promedio de quices es menor a 3
     * @return un vector de objetos de la clase Estudiante
     */
    private int indexPerdidos(){
        int index = 0;
        for(int i = 0; i < this.listaEstructuras.length; i++){
            float promedioEstudiante = this.listaEstructuras[i].promedio();
            if(promedioEstudiante < 3.0F){
                index++;
            }
        }
        return index;
    }
    public Estudiante[] getReprobaronQuices()
    {
        Estudiante[] estudiantesPerdieron = new Estudiante[this.indexPerdidos()];
        int index = 0;
        for(int i = 0; i < this.listaEstructuras.length; i++){
            Estudiante estudiante = this.listaEstructuras[i];
            float promedioEstudiante = estudiante.promedio();
            if(promedioEstudiante < 3.0F){
                estudiantesPerdieron[index++] = estudiante;
            }
        }
        return estudiantesPerdieron;
    }

    
    
    
    /**
     * Obtiene los estudiantes cuyo promedio de quices es mayor o igual a 4
     * @return un vector de objetos de la clase Estudiante
     */
//Literal es el mismo metodo de arriba, solo cambian las reglas del if
    public Estudiante[] getMayorNotaQuices()
    {
        Estudiante[] mayorNota = new Estudiante[this.indexPerdidos()];
        int index = 0;
        for(int i = 0; i < this.listaEstructuras.length; i++){
            Estudiante estudiante = this.listaEstructuras[i];
            float promedioEstudiante = estudiante.promedio();
            if(promedioEstudiante >= 4.0F){
                mayorNota[index++] = estudiante;
            }
        }
        return mayorNota;
    }
    
    /**
     * Obtiene el nombre del quiz que más perdieron los estudiantes (q1, q2....qn)
     * @return un String con el nombre de la columna en Excel
     */
    public String getNombreQuiz_Perdieron()
    {
        Estudiante[] nombreL = new Estudiante[this.indexPerdidos()];
        int index = 0;
        for(int i = 0; i < this.listaEstructuras.length; i++){
            Estudiante estudiante = this.listaEstructuras[i];
            float promedioEstudiante = estudiante.promedio();
            if(promedioEstudiante < 3.0F){
                nombreL[index++] = estudiante;
            }
        }
        return nombreL.toString();
    }
    /**
     * Obtiene la nota que más se repite  (Ojo suponga notas de un entero y un decimal
     * @return un float con la nota que más se repite
     */
/*  En este metodo se busca agrupar todos los quices en una única lista, para esto
 *  para esto se utiliza un array bidimensional 
 */
    
    private float[][] singleArray(){
        float [][] notas = new float[this.listaEstructuras.length][];
        for(int i = 0; i < this.listaEstructuras.length; i++){
            Estudiante estudiante = this.listaEstructuras[i];
            notas[i] = new float[estudiante.getQuices().length];
            for(int j = 0; j < notas[i].length; j++){
                notas[i][j] = estudiante.getQuices()[j];
            }
        }
        return notas;
    }
    
    private int xRepeated(float nota, float [][] singleArray){
        float [][] notas = this.singleArray();
        int cont= 0;
        for(int i = 0; i < notas.length; i++){
            for(int j = 0; j < notas[i].length; j++){
                if(nota == notas[i][j])
                    cont++;
            }
        }
        return cont;
    }  
    
    public float getNota_Que_MasRepite(){
        
        float greater = 0;
        int mRepetida = 0;
        int qRepeat = 0;
        
        float [][] notas = this.singleArray();
        //Recorre los estudiantes, y cada uno de sus quices
        for(int i = 0; i < notas.length; i++){
            for(int j = 0; j < notas[i].length; j++){
                float nota = notas[i][j];
                qRepeat = this.xRepeated(nota, notas);
                //Establece la mayor repetida y compara con la siguiente
                if(qRepeat > mRepetida){
                    greater = nota;
                    mRepetida = qRepeat;
                }
            }
        }
        return greater;
    }
}